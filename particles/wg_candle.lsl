// ©web.gearbox, 2011. http://webgearbox.tumblr.com

float scale=0.1;

float multipler=1.6;
key texture="cdc3f17d-9d10-c286-cf98-fb516253cbc3";
integer owner_only=0;

list f(){return[0,272,9,4,16,1,22,0,23,3.141593,1,<1, 1, 1>,3,<1, 1, 1>,2,0.4,4,0.01,5,<scale, scale*multipler, 0>,6,<0.05, 0.09, 0>,12,texture,19,0,7,1,13,0.1,15,1,8,<0, 0, 0>,21,<0, 0, 0>,17,0.001,18,0.003];}
    
list s(){return [0,275,9,8,16,0,22,0.2,23,0.01,1,<0.8, 0.8, 0.8>,3,<0, 0, 0>,2,0.3,4,0.1,5,<0.5*scale, 0.5*scale, 0>,6,<0.5*scale, 10*scale, 0>,12,"9cb16d78-608d-a5a0-253a-09501921ddf3",19,0,7,2,13,0.2,15,1,8,<0, 0, 0>,21,<0, 0, 0>,17,0.01,18,0.01];}
    
integer on=1;

default
{
    state_entry()
    {
         llParticleSystem(f());
    }
    touch_start(integer i){
        if(owner_only)if(llDetectedKey(0)!=llGetOwner())return;    
        
        on=!on;
        if(on)llParticleSystem(f());
        else{
            llParticleSystem(s());            
            llSleep(2);
            llParticleSystem([]);
        }
        
    }
}
