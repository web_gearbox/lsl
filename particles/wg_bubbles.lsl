// ©web.gearbox, 2013. http://webgearbox.tumblr.com

particles(vector color){
    
    thecolor=color;
    llParticleSystem([
        0,291,9,8,16,0,22,0,23,1,1,color,3,color,
        2,1,4,0,5,<0, 0, 0>,6,<1, 1, 0>,
        12,"8f958713-8277-c418-82b5-0c41b42135df",
        19,0,7,10,13,0.1,15,1,8,<0, 0, 0.1>,
        21,<0, 0, 0>,17,0.01,18,0.05]
    );   
    
    
}

rotation spin=<0.02886, 0.02886, 0.02886, 0.99875>;
vector thecolor=<1,0.5,0.5>;

default
{
    state_entry(){
       llSetTimerEvent(0.1);
    }
    timer(){
       particles(thecolor*spin); 
    }
}
