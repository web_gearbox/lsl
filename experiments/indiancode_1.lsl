// write more - do less!

float ln(float value,float base){return llLog(value)/llLog(base);}    
integer integerLog2(float value){return (integer)ln(value,2);}
    

default
{
    state_entry(){
        integer n=10;
        integer i=1;
        while(n--){
            i = i<<1;
            llOwnerSay((string)(integerLog2(i)));           
        }
    }
}
