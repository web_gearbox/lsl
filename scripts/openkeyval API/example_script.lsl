integer MODE_SET   = 0x9E7;
integer MODE_GET   = 0x5E7;

integer MODE_GET_R = 0xCAC;
integer MODE_SET_R = 0x1A1;

integer MODE_ERR   = 0xBAD;

string var_name="ebat_ty_lokh";
string data="This is some data. Even with unicode: \"Чуток уникода\".";

default
{

    touch_start(integer total_number){
        
       llMessageLinked(-1,MODE_SET,data,var_name);
    }
    
    link_message(integer prim,integer num,string data,key id){
        
        if(num==MODE_SET_R){
            llOwnerSay("Variable "+var_name+" setted succesfully. Retrieving data from server...");
            llMessageLinked(-1,MODE_GET,"",id);
        }
        else if(num==MODE_GET_R){
            llOwnerSay(var_name+" variable name returned from server:");
            llOwnerSay(data);
        }
        else if(num==MODE_ERR){
            llOwnerSay("Shit happens. Error setting variable "+var_name+"!. Error code:");
            llOwnerSay(data);
        }
        
    }
    
}
