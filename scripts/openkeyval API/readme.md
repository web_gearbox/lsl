# OpenKeyval API for LSL
version 1.0

Welcome. This is a simple API that allows to store and retrieve variables on <http://openkeyval.org/> service. OpenKeyval is a key-value storage, which goal is to provide free, simple to use and humanly data storage. In order to make API work - put openkeyval.api script in linkset your script is in and send it a linkmessage. API will answer via linkmessage too.

## Constants

There are several constants you should set for ease of script use. Here they are:

    integer MODE_SET   = 0x9E7;   integer MODE_GET   = 0x5E7;
    integer MODE_SET_R = 0x1A1;   integer MODE_GET_R = 0xCAC;
    integer MODE_ERR   = 0xBAD;

Just copypaste them to the beginning of your script (like in example script)

## Responses

The API will return link\_message data (sent at whole linkset). Key part of it will usually be the name of the variable, integer part will be MODE\_SET\_R when API is reporting about succesful variable set, MODE\_GET\_R when the variable name is retrieved from server and MODE\_ERR in case of error.

## Request modes

- MODE_SET - Set a value. Key part is name of variable, string part is the value you wish to set:

    llMessageLinked(-1,MODE_SET,"Data to set","variable");
    

- MODE_SET - Add some text to paste data.:

    llMessageLinked(-1,MODE_GET,"","variable");
    
## Response modes

 - MODE\_SET\_R - Returned when setting variable value vas succesful. Key part is variable name.

 - MODE\_GET\_R - Returned when getting variable value vas succesful. String part is value of the variable, which name is passed in key part.

 - MODE\_ERR - Returned on error. String part will be the error code and error text if any. key part will be the name of the variable

## LSL code Example

    integer MODE_SET= 0x9E7;      integer MODE_GET= 0x5E7;
    integer MODE_SET_R = 0x1A1;   integer MODE_GET_R = 0xCAC;
    integer MODE_ERR= 0xBAD;

    string var_name="variable";
    string data="This is some data. Even with unicode: \"Чуток уникода\".";
    
    default{
        touch_start(integer total_number){ 

            // On touch - setting var_name variable equal to data
            llMessageLinked(-1,MODE_SET,data,var_name);

        }
        link_message(integer prim,integer num,string data,key id){

            // The api answered that setting variable was succesful
            if(num==MODE_SET_R){
                llOwnerSay("Variable "+var_name+" setted succesfully. Retrieving data from server...");
                // Reading variable we set from server:
                llMessageLinked(-1,MODE_GET,"",var_name);
            }

            // The api answered that getting variable was succesful
            else if(num==MODE_GET_R){
                llOwnerSay(var_name+" variable name returned from server:");
                llOwnerSay(data);
            }

            // The api reported error
            else if(num==MODE_ERR){
                llOwnerSay("Shit happens. Error setting variable "+var_name+"!. Error code:");
                llOwnerSay(data);
            }

        }
    }


## Restrictions and stuff

 - The variable value size is limited by 8k. Theres no check so its not a strict rule, but something bad may happen if you exceed it.
 - Latin characters, numbers, dashes and underscores are allowed for variable names.
 - Variable names are case-sensitive
 - Variable names must be of 5 to 36 symbols long.
 - Data is (should be) binary-safe. So unicode chars should be just fine.

## Credits

This API was written by Web Gearbox on 2013.11.27.
