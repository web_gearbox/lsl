// Particle system script version 0.2
// (c) web.gearbox, 2013 http://webgearbox.tumblr.com

// Source

vector angles           = <0,PI,0>;
float radius            = 2;
vector speed            = <0,0,0>;
vector omega            = ZERO_VECTOR;
float source_age        = 0;       // Note : source age is currently unstable and pretty buggy.  

integer count           = 1;

float rate              = 2;
float particle_age      = 2;    
                        

integer pattern      // = PSYS_SRC_PATTERN_EXPLODE;
                     // = PSYS_SRC_PATTERN_ANGLE_CONE;
                     // = PSYS_SRC_PATTERN_ANGLE;
                     = PSYS_SRC_PATTERN_DROP;
                     // = PSYS_SRC_PATTERN_ANGLE_CONE_EMPTY;
                     
integer bf_source       = PSYS_PART_BF_SOURCE_ALPHA;
                     // = PSYS_PART_BF_ONE;
                     // = PSYS_PART_BF_ZERO;
                     // = PSYS_PART_BF_DEST_COLOR;
                     // = PSYS_PART_BF_SOURCE_COLOR;
                     // = PSYS_PART_BF_ONE_MINUS_DEST_COLOR;
                     // = PSYS_PART_BF_ONE_MINUS_SOURCE_COLOR;
                     // = PSYS_PART_BF_SOURCE_ALPHA;
                     // = PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA;
                        


integer bf_dest      = PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA;
                     // = PSYS_PART_BF_ONE;
                     // = PSYS_PART_BF_ZERO;
                     // = PSYS_PART_BF_DEST_COLOR;
                     // = PSYS_PART_BF_SOURCE_COLOR;
                     // = PSYS_PART_BF_ONE_MINUS_DEST_COLOR;
                     // = PSYS_PART_BF_ONE_MINUS_SOURCE_COLOR;
                     // = PSYS_PART_BF_SOURCE_ALPHA;
         
key target              = "f0980b5a-13cd-4eb0-ae62-8ea5fc58a5da";
integer moving_to_target       = FALSE;
integer linear          = FALSE;

integer wind            = FALSE;

integer bounce          = FALSE;
vector accel            = <0,0,0>;
integer attached        = TRUE;
integer ribbon          = FALSE;
integer targeted        = FALSE;

// Particle

integer interp_color    = FALSE;
rotation color_start    = <1,1,1,1>;
rotation color_end      = <1,1,1,1>;
float glow_start        = 0;
float glow_end          = 1;

integer interp_scale    = FALSE;
vector scale_start      = <4,4,0>;
vector scale_end        = <4,4,0>;

integer fullbright      = TRUE;
key texture           //  = "34e5a25f-6503-3ff3-0a23-d4d32f47c9c3";
                      //  = "2b6fc8ae-ec64-0161-6c29-15e4199a583e";
                          = NULL_KEY;



// ---- Don't touch stuff below unless you know what you're doing

float bound(float s,float min,float max){
    if(s<min)return min;
    if(s>max)return max;
    return s;
}

integer number_current (){
    float rx=rate;if(rx==0)rx=0.001;
    return llCeil(particle_age * count / rate);
}

integer number(list s){

    list flags=llList2ListStrided(s, 0, -1, 2);

    float br;
    float age;
    float cnt;

    integer f=llListFindList(flags, [PSYS_SRC_BURST_RATE]);
    if(~f)br=llList2Float(s, f*2+1);else br=1;

    f=llListFindList(flags, [PSYS_PART_MAX_AGE]);
    if(~f)age=llList2Float(s, f*2+1);else age=1;

    f=llListFindList(flags, [PSYS_SRC_BURST_PART_COUNT]);
    if(~f)cnt=llList2Integer(s, f*2+1);else age=1;

    if(br==0)br=0.001;

    return llCeil(age * cnt / br);
}

list assemble_brute(){
    return [
       PSYS_SRC_ANGLE_BEGIN,angles.x,
       PSYS_SRC_ANGLE_END,angles.y,
       PSYS_SRC_BURST_RADIUS,radius,
       PSYS_PART_START_COLOR,<color_start.x,color_start.y,color_start.z>,
       PSYS_PART_START_ALPHA,color_start.s,
       PSYS_PART_END_COLOR,<color_end.x,color_end.y,color_end.z>,
       PSYS_PART_END_ALPHA,color_end.s,
       PSYS_SRC_PATTERN, pattern,
       PSYS_SRC_OMEGA, omega,
       PSYS_SRC_ACCEL, accel,
       PSYS_PART_MAX_AGE, particle_age,
       PSYS_SRC_MAX_AGE, source_age,
       PSYS_SRC_TEXTURE, texture,
       PSYS_PART_START_SCALE, scale_start,
       PSYS_PART_END_SCALE, scale_end,
       PSYS_SRC_BURST_SPEED_MIN,speed.x,
       PSYS_SRC_BURST_SPEED_MAX,speed.y,
       PSYS_SRC_TARGET_KEY,target,
       PSYS_SRC_BURST_RATE, rate,
       PSYS_SRC_BURST_PART_COUNT, count,
       PSYS_PART_BLEND_FUNC_SOURCE, bf_source,
       PSYS_PART_BLEND_FUNC_DEST, bf_dest,       
       PSYS_PART_FLAGS, 
         PSYS_PART_BOUNCE_MASK * bounce +
         PSYS_PART_EMISSIVE_MASK * fullbright +
         PSYS_PART_FOLLOW_SRC_MASK * attached +
         PSYS_PART_FOLLOW_VELOCITY_MASK * targeted + 
         PSYS_PART_INTERP_COLOR_MASK * interp_color +
         PSYS_PART_INTERP_SCALE_MASK * interp_scale +
         PSYS_PART_RIBBON_MASK * ribbon +
         PSYS_PART_TARGET_LINEAR_MASK * linear +
         PSYS_PART_TARGET_POS_MASK * moving_to_target +
         PSYS_PART_WIND_MASK * wind,

        PSYS_PART_START_GLOW, glow_start,
        PSYS_PART_END_GLOW, glow_end

    ];
}

list assemble_smart(){
    list p;

    if(pattern == PSYS_SRC_PATTERN_ANGLE_CONE |
       pattern == PSYS_SRC_PATTERN_ANGLE_CONE_EMPTY |
       pattern == PSYS_SRC_PATTERN_ANGLE
    )
    p+=[PSYS_SRC_ANGLE_BEGIN,angles.x,PSYS_SRC_ANGLE_END,angles.y];

    if(pattern !=PSYS_SRC_PATTERN_DROP && !attached && !linear)
    p+=[PSYS_SRC_BURST_RADIUS, radius];

    if(color_start!=<1,1,1,1>)
    p+=[PSYS_PART_START_COLOR,<color_start.x,color_start.y,color_start.z>,
        PSYS_PART_START_ALPHA,color_start.s];

    if(interp_color && color_start != color_end)
    p+=[PSYS_PART_END_COLOR,<color_end.x,color_end.y,color_end.z>,
       PSYS_PART_END_ALPHA,color_end.s];

    if(omega!=ZERO_VECTOR)p+=[PSYS_SRC_OMEGA,omega];

    p+=[PSYS_SRC_PATTERN, pattern];

    if(accel!=ZERO_VECTOR && !linear)p+=[PSYS_SRC_ACCEL,<bound(accel.x,-100,100),bound(accel.y,-100,100),bound(accel.z,-100,100)>];

    p+=[PSYS_PART_MAX_AGE,particle_age];

    if(source_age)p+=[PSYS_SRC_MAX_AGE, source_age];
    if(texture)p+=[PSYS_SRC_TEXTURE,texture];

    p+=[PSYS_PART_START_SCALE, <bound(scale_start.x,0.03125,4),bound(scale_start.y,0.03125,4),0>];
    if(interp_scale)p+=[PSYS_PART_END_SCALE, <bound(scale_end.x,0.03125,4),bound(scale_end.y,0.03125,4),0>];

    if(pattern!=PSYS_SRC_PATTERN_DROP){
        if(speed.y>speed.x){
            p+=[PSYS_SRC_BURST_SPEED_MIN,speed.x,PSYS_SRC_BURST_SPEED_MAX,speed.y];
        }
        else p+=[PSYS_SRC_BURST_SPEED_MIN,speed.y];
    }

    p+=[PSYS_SRC_BURST_RATE, rate];
    p+=[PSYS_SRC_BURST_PART_COUNT, count];

    if(bf_source && bf_source!=PSYS_PART_BF_SOURCE_ALPHA)p+=[PSYS_PART_BLEND_FUNC_SOURCE, bf_source];
    if(bf_dest && bf_dest!=PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA)p+=[PSYS_PART_BLEND_FUNC_DEST, bf_dest];   

    if(moving_to_target){
        p+=[PSYS_SRC_TARGET_KEY, target];
    }
    if(glow_start)p+=[PSYS_PART_START_GLOW, glow_start];
    if(glow_end)p+=[PSYS_PART_END_GLOW, glow_end];

    p+=[PSYS_PART_FLAGS, 
         PSYS_PART_BOUNCE_MASK * bounce * (!linear) +
         PSYS_PART_EMISSIVE_MASK * fullbright +
         PSYS_PART_FOLLOW_SRC_MASK * attached +
         PSYS_PART_FOLLOW_VELOCITY_MASK * targeted  + 
         PSYS_PART_INTERP_COLOR_MASK * interp_color +
         PSYS_PART_INTERP_SCALE_MASK * interp_scale +
         PSYS_PART_RIBBON_MASK * ribbon +
         PSYS_PART_TARGET_LINEAR_MASK * linear +
         PSYS_PART_TARGET_POS_MASK * moving_to_target +
         PSYS_PART_WIND_MASK * wind * (!linear)
    ];

    return p;

}

// -- Functions for particle system presentation as a copypaste-ready list declaration

string floatAsString(float a){
  string s=(string)a;
  while(llGetSubString(s,-1,-1) =="0"){
      s=llGetSubString(s,0,-2);
      if(llGetSubString(s,-1,-1) ==".")return llGetSubString(s,0,-2);
  }
  return s;
}

string vectorAsString(vector a){
    return "<"+floatAsString(a.x)+", "+floatAsString(a.y)+", "+floatAsString(a.z)+">";  
}

string rotationAsString(rotation a){
    return "<"+floatAsString(a.x)+", "+floatAsString(a.y)+", "+floatAsString(a.z)+", "+floatAsString(a.s)+">";  
}

string stringAsString(string a){
    return "\""+llDumpList2String(llParseStringKeepNulls(llDumpList2String(llParseStringKeepNulls(llDumpList2String(llParseStringKeepNulls(llDumpList2String(llParseStringKeepNulls(a,["\\"],[]),"\\\\"),["\""],[]),"\\\""),["\t"],[]),"\\t"),["\n"],[]),"\\n")+"\"";       
    
}

list constants=[
    PSYS_PART_FLAGS, "PSYS_PART_FLAGS",
    PSYS_PART_START_COLOR, "PSYS_PART_START_COLOR",
    PSYS_PART_START_ALPHA, "PSYS_PART_START_ALPHA",    
    PSYS_PART_END_COLOR, "PSYS_PART_END_COLOR",
    PSYS_PART_END_ALPHA, "PSYS_PART_END_ALPHA",
    PSYS_PART_START_SCALE, "PSYS_PART_START_SCALE",
    PSYS_PART_END_SCALE, "PSYS_PART_END_SCALE",
    PSYS_PART_MAX_AGE, "PSYS_PART_MAX_AGE",    
    PSYS_SRC_ACCEL, "PSYS_SRC_ACCEL",    
    PSYS_SRC_PATTERN, "PSYS_SRC_PATTERN",    
    PSYS_SRC_TEXTURE, "PSYS_SRC_TEXTURE",        
    PSYS_SRC_BURST_RATE, "PSYS_SRC_BURST_RATE",
    PSYS_SRC_BURST_PART_COUNT, "PSYS_SRC_BURST_PART_COUNT",
    PSYS_SRC_BURST_RADIUS, "PSYS_SRC_BURST_RADIUS",
    PSYS_SRC_BURST_SPEED_MIN, "PSYS_SRC_BURST_SPEED_MIN",
    PSYS_SRC_BURST_SPEED_MAX, "PSYS_SRC_BURST_SPEED_MAX",
    PSYS_SRC_MAX_AGE, "PSYS_SRC_MAX_AGE",    
    PSYS_SRC_TARGET_KEY, "PSYS_SRC_TARGET_KEY",
    PSYS_SRC_OMEGA, "PSYS_SRC_OMEGA",    
    PSYS_SRC_ANGLE_BEGIN, "PSYS_SRC_ANGLE_BEGIN",
    PSYS_SRC_ANGLE_END, "PSYS_SRC_ANGLE_END",    
    PSYS_PART_BLEND_FUNC_SOURCE, "PSYS_PART_BLEND_FUNC_SOURCE",
    PSYS_PART_BLEND_FUNC_DEST, "PSYS_PART_BLEND_FUNC_DEST",    
    PSYS_PART_START_GLOW, "PSYS_PART_START_GLOW",
    PSYS_PART_END_GLOW, "PSYS_PART_END_GLOW"
];

list constants_blend=[
    PSYS_PART_BF_ONE, "PSYS_PART_BF_ONE",
    PSYS_PART_BF_ZERO, "PSYS_PART_BF_ZERO",
    PSYS_PART_BF_DEST_COLOR, "PSYS_PART_BF_DEST_COLOR",
    PSYS_PART_BF_SOURCE_COLOR, "PSYS_PART_BF_SOURCE_COLOR",
    PSYS_PART_BF_ONE_MINUS_DEST_COLOR, "PSYS_PART_BF_ONE_MINUS_DEST_COLOR",
    PSYS_PART_BF_ONE_MINUS_SOURCE_COLOR, "PSYS_PART_BF_ONE_MINUS_SOURCE_COLOR",
    PSYS_PART_BF_SOURCE_ALPHA, "PSYS_PART_BF_SOURCE_ALPHA",
    PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA, "PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA"
];

list constants_flags=[
    PSYS_PART_BOUNCE_MASK,"PSYS_PART_BOUNCE_MASK",
    PSYS_PART_EMISSIVE_MASK, "PSYS_PART_EMISSIVE_MASK",
    PSYS_PART_FOLLOW_SRC_MASK, "PSYS_PART_FOLLOW_SRC_MASK",
    PSYS_PART_FOLLOW_VELOCITY_MASK, "PSYS_PART_FOLLOW_VELOCITY_MASK",
    PSYS_PART_INTERP_COLOR_MASK, "PSYS_PART_INTERP_COLOR_MASK",
    PSYS_PART_INTERP_SCALE_MASK, "PSYS_PART_INTERP_SCALE_MASK",
    PSYS_PART_RIBBON_MASK, "PSYS_PART_RIBBON_MASK",
    PSYS_PART_TARGET_LINEAR_MASK, "PSYS_PART_TARGET_LINEAR_MASK",
    PSYS_PART_TARGET_POS_MASK, "PSYS_PART_TARGET_POS_MASK",
    PSYS_PART_WIND_MASK, "PSYS_PART_WIND_MASK"
];

list constants_pattern=[
    PSYS_SRC_PATTERN_EXPLODE, "PSYS_SRC_PATTERN_EXPLODE",
    PSYS_SRC_PATTERN_ANGLE_CONE, "PSYS_SRC_PATTERN_ANGLE_CONE",
    PSYS_SRC_PATTERN_ANGLE, "PSYS_SRC_PATTERN_ANGLE",
    PSYS_SRC_PATTERN_DROP, "PSYS_SRC_PATTERN_DROP"

];

string flagsAsString(integer flags){
    list fl;
    integer i=llGetListLength(constants_flags)/2;
    while(i--)if(flags&llList2Integer(constants_flags,i*2))fl+=llList2String(constants_flags,i*2+1);
    return llDumpList2String(fl, " | ");
}

string psysAsString(list src, integer consts){
        
    integer num=llGetListLength(src);
    integer i;
    list r=[];
    for(i=0;i<num;i++){
       
        integer type=llGetListEntryType(src,i);
        if(type==TYPE_FLOAT)r+=floatAsString(llList2Float(src,i));
        else if(type==TYPE_INTEGER){
            if(consts){
                integer rule=llList2Integer(src,i);
                if(!(i%2)){
                    integer f=llListFindList(constants,[rule]);
                    if(~f)r+=llList2String(constants,f+1);
                    else r+=(string)rule;
                }
                else{
                    integer prevRule=llList2Integer(src,i- 1);
                    if(prevRule==PSYS_PART_BLEND_FUNC_SOURCE | prevRule==PSYS_PART_BLEND_FUNC_DEST){
                        integer f=llListFindList(constants_blend,[rule]);
                        if(~f)r+=llList2String(constants_blend,f+1);
                        else r+=(string)rule;                        
                    }                
                    else if(prevRule==PSYS_SRC_PATTERN){
                        integer f=llListFindList(constants_pattern,[rule]);
                        if(~f)r+=llList2String(constants_pattern,f+1);
                        else r+=(string)rule;          
                    }
                    else if(prevRule==PSYS_PART_FLAGS){
                        r+=flagsAsString(rule);
                    }
                    else r+=(string)rule;
                }
            }
            else r+=(string)llList2Integer(src,i);
        }
        else if(type==TYPE_VECTOR)r+=vectorAsString(llList2Vector(src,i));
        else if(type==TYPE_ROTATION)r+=rotationAsString(llList2Rot(src,i));  
        else if(type==TYPE_KEY || type==TYPE_STRING)r+=stringAsString(llList2String(src,i));
       
    }    
    
    return "["+llDumpList2String(r,", ")+"]";
    
}

// -- Particle system storing and generating

list psys;
generate(){
    psys=assemble_smart();    
}

// -- Dialog

integer wgDlgChannel=31337;
integer wgListenHdl;

wgDialog(key id,string text,list buttons){
 
    wgDlgChannel=-(integer)llFrand(20000+10000);
    llListenRemove(wgListenHdl);
    wgListenHdl=llListen(wgDlgChannel,"",id,"");
    llDialog(id,text,buttons,wgDlgChannel);
    
}

/* -- Declaration of stuff for lslint (i hate it yelling at me and wait for its update still) 

integer PSYS_PART_START_GLOW = 26;
integer PSYS_PART_END_GLOW   = 27;

integer PSYS_PART_BLEND_FUNC_SOURCE = 24;
integer PSYS_PART_BLEND_FUNC_DEST   = 25;

integer PSYS_PART_BF_ONE              = 0x0;
integer PSYS_PART_BF_ZERO             = 0x1;
integer PSYS_PART_BF_DEST_COLOR          = 0x2;
integer PSYS_PART_BF_SOURCE_COLOR      = 0x3;
integer PSYS_PART_BF_ONE_MINUS_DEST_COLOR   = 0x4;
integer PSYS_PART_BF_ONE_MINUS_SOURCE_COLOR = 0x5;
integer PSYS_PART_BF_SOURCE_ALPHA      = 0x7;
integer PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA = 0x9;

integer PSYS_PART_RIBBON_MASK          = 0x400;*/



default
{
    state_entry()
    {
        generate();
        llLinkParticleSystem(!!llGetNumberOfPrims(),psys);
    }
    touch_start(integer num){
        if(llDetectedKey(0)!=llGetOwner())return;
        wgDialog(llDetectedKey(0),"What type?",["brute","smart","-","c-brute","c-smart","-"]);  
    }
    
    listen(integer lsn_channel,string lsn_name,key lsn_key,string lsn_msg)
    {
        llListenRemove(wgListenHdl);
        
        if(lsn_msg=="brute")llOwnerSay(psysAsString(assemble_brute(),0));
        else if(lsn_msg=="smart")llOwnerSay(psysAsString(assemble_smart(),0));
        else if(lsn_msg=="c-brute")llOwnerSay(psysAsString(assemble_brute(),1));
        else if(lsn_msg=="c-smart")llOwnerSay(psysAsString(assemble_smart(),1));        
        
    }    
}