integer channel_system=-50050;

default{
    
    state_entry(){
        
        llListen(-50050,"",NULL_KEY,"");
        
    }
    listen(integer chan,string name,key id,string msg){
        if(llGetOwnerKey(id)!=llGetOwner())return;
        
        if(msg=="show"){
            llSetLinkAlpha(-1,1,-1);    
        }
        else if(msg=="hide"){
            llSetLinkAlpha(-1,0,-1);    
        }        
    }
}
