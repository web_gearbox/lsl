//©web.gearbox, 2012. http://webgearbox.tumblr.com
// If you paid a penny for this script - the seller is an asshole.

string animationame="";//Name of animation to play. If empty the default animation will be played.

float arc=180;//Attack arc in degrees.

float radius=3;//Radius of attack

float delay=0.5;//Delay between hits.

integer ignore_touch=0;//Ignore touches
integer ignore_chat=0;//Ignore chat commands (huds too)

integer allow_animation=1;
integer allow_sounds=1;
integer allow_status=0;//To say "weapon drawn/sheated" in chat

integer report_hits=0;//To report people hitted to owner;

string damagetype="sword";//Damage type
string damagetype1="sword"; //Old damage type

//sword: arrow, arrow~direct, dart, dart~direct, sword, spear, dagger, dagger~direct, whip, swhip axe or hammer, pickaxe, shammer;kick: slave, slave~direct;punch or kick;trap;great: bullet, bullet~direct, cannon

integer channel_chat=556;
integer channel_system=-50050;

list swings=[
    "ebc07f62-0bbe-ce5d-8b54-b68abe3e86bb",
    "2721ef15-8e4a-f91a-a53a-f1e2d9ee0058",
    "ff94dd2c-0f7a-6ff1-dc52-bcc0b2061fd5",
    "5bac936e-93ad-9e77-8a9c-0ba3ca0b72d9",
    "0938f37b-2ba8-724a-5dc3-a3f5ef7413ed"
];
list hits=[
    "0e4610a3-3e1a-66ed-d929-68781fa768d3",
    "93a32391-d620-1752-7d66-b55957b85f1b",
    "57a47d9a-f43b-08b8-6cc4-5c516f39ded5",
    "b33ab342-0673-52d5-ef49-94825b726ed5",
    "34f215da-b066-cdd7-c5b9-86433f63d311",
    "720a48f6-9811-8ee8-fe67-a19fc10fa971",
    "b8d512b5-ad18-d91d-6187-7ec88bfe7dd5",
    "dba86fc3-d68c-8df8-3558-5fc8ef10f70e"

];


































swing(){
 
    if(!allow_sounds)return;
    integer i=llGetListLength(swings);   
    if(i)llPlaySound(llList2String(swings,(integer)llFrand(i)),1);
    
}

hit(){
    if(!allow_sounds)return;
    integer i=llGetListLength(hits);   
    if(i)llPlaySound(llList2String(hits,(integer)llFrand(i)),1);
    
}

integer hidden=1;
string anima;

showhide(){ //это очень сильное колдунство..
 
    hidden=!hidden;  
    llMessageLinked(-1,!hidden,"showhide",NULL_KEY);
    llSay(channel_system,llList2String(["show","hide"],hidden));
    if(allow_status)llOwnerSay("Weapon is "+llList2String(["drawn","sheated"],hidden));
    if(hidden){
     
        llRequestPermissions(llGetOwner(),0);
        llReleaseControls();
        return;
        
    }
    
    if(llGetAttached())
    llRequestPermissions(llGetOwner(),PERMISSION_TAKE_CONTROLS | PERMISSION_TRIGGER_ANIMATION);
    
    
}

default{
    
    changed(integer change){  
        if(change & CHANGED_OWNER){
            llResetScript();         
        }       
        if(change & CHANGED_INVENTORY){
            if(llGetInventoryType(animationame)==INVENTORY_ANIMATION)anima=animationame;
            else anima="sword_strike_r";         
        }
    }
    
    touch_start(integer num){
        if(ignore_touch)return;
        if(llDetectedKey(0)!=llGetOwner())return;
        showhide();
        
    }
    
    run_time_permissions(integer perms){
        
        
        if(perms & PERMISSION_TAKE_CONTROLS){
            llTakeControls(CONTROL_ML_LBUTTON | CONTROL_LBUTTON, TRUE, FALSE);
        }

    }

    state_entry(){
        
        if(llGetInventoryType(animationame)==INVENTORY_ANIMATION)anima=animationame;
        else anima="sword_strike_r";         
        if(ignore_chat)return;
        llListen(channel_chat, "", "", "" );

    }   
    
    
    listen(integer channel,string name,key id,string message){
        
        if(llGetOwnerKey(id)!=llGetOwner())return;
        if(message=="drawsheath"){      
            showhide();
        }
    }
    
    sensor(integer num){

        if(hidden)return;        
        llMessageLinked(-1,1,"strike",NULL_KEY);              
        while(num--){
            llRegionSayTo(llDetectedKey(num),(integer)("0x" + llGetSubString((string)llDetectedKey(num),0,7)), damagetype1+"," + llKey2Name(llGetOwner()) + "," + llDetectedName(num)+","+damagetype);
            if(report_hits)llOwnerSay("Hit at "+llDetectedName(num));
        }
        hit();

        
    }
    
    no_sensor(){
        swing();
        llMessageLinked(-1,0,"strike",NULL_KEY);          
    }
    
    control(key id,integer held,integer change){
        
        integer pressed = held & change;
        integer down = held & ~change;
        integer released = ~held & change;
        integer inactive = ~held & ~change;        
        
        
        if((released&CONTROL_ML_LBUTTON) | (released%CONTROL_LBUTTON))return;
        if(llGetTime()<delay)return;
        if(hidden)return;
        if(allow_animation)llStartAnimation(anima);   
        llSensor("","",AGENT,radius,(arc*DEG_TO_RAD)/2); 
        llMessageLinked(-1,1,"swing",NULL_KEY);
        llResetTime(); 
    }

}