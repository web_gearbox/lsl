// ©web.gearbox, 2012 http://webgearbox.tumblr.com

unsit(){integer c=llGetObjectPrimCount(llGetKey());integer N=llGetNumberOfPrims();while(N>c)llUnSit(llGetLinkKey(N--));}

drop(vector pos){if(pos==ZERO_VECTOR){unsit();return;}vector start=llGetPos();llSetRegionPos(pos);unsit();llSetRegionPos(start);}   

teleport(){
    string desc=llGetObjectDesc();string s=llGetSubString(desc,0,0);if(s=="+")drop(llGetPos()+(vector)llGetSubString(desc,1,-1));else if(s=="*")drop(llGetPos()+(vector)llGetSubString(desc,1,-1)*llGetRot());else drop((vector)desc);
}

default{
    state_entry(){llSetClickAction(CLICK_ACTION_SIT);llSitTarget(<0,0,0.1>,ZERO_ROTATION);llSetMemoryLimit(9000);}
    changed(integer change){
        if((change&CHANGED_LINK) && llGetNumberOfPrims()>llGetObjectPrimCount(llGetKey()))teleport();
    }
}
