create(){
    llMessageLinked(-1,0,"8a38123a86e042a6770b11a89ff24cd7","key");
    llMessageLinked(-1,0,"lsl2","format");
    llMessageLinked(-1,0,"","clear");  
    llMessageLinked(-1,0,"10M","expiry"); 
    llMessageLinked(-1,0,page,"add");
    llMessageLinked(-1,0,"wg_rerezzer script","create");    
}  

string page="";

string vecty(string in){
    
    list p=llParseString2List(in,[",","<",">"],[]);
    integer i=llGetListLength(p);
    list n=[];
    while(i--){
     
        n=floaty(llList2String(p,i))+n;   
        
    }
    return "<"+llDumpList2String(n,",")+">";
    
}

string floaty(string in){
    
    while(llGetSubString(in,-1,-1)=="0")in=llGetSubString(in,0,-2);
    if(llGetSubString(in,-1,-1)==".")in=llGetSubString(in,0,-2);
    return in;
    
}


string end="];

default{

    touch_start(integer total_number)
    {
        string child=llGetInventoryName(INVENTORY_OBJECT,0);
        if(llGetInventoryType(child)!=INVENTORY_OBJECT){
            llOwnerSay(\"First you must take the \\\"child\\\" script, put it in your own prim, then put the prim in this object' contents\");
            return;
        }
        integer n=llGetListLength(prims);
        while(n--){
            llRezObject(child,llGetPos(),ZERO_VECTOR,ZERO_ROTATION,1000+n);
            llRegionSay(1000+n,llList2String(prims,n));
        }
    }
}";

generate(){
    
    page="list prims=[";
    integer n=llGetNumberOfPrims()+1;
    integer pp=0;
    while(--n){
        llSetText((string)n+" left",<1,1,1>,1);        
        list p=llGetLinkPrimitiveParams(n,[PRIM_POSITION,PRIM_ROTATION,PRIM_SIZE,PRIM_TEXTURE,0,PRIM_TYPE]);
        if(llList2Integer(p,7)==PRIM_TYPE_SCULPT){
            if(pp)page+=",\n";                        
            else pp=1;
            page+="\""+llDumpList2String([llList2String(p,8),llList2Integer(p,9),vecty((string)(llList2Vector(p,0)-llGetPos())),vecty(llList2String(p,1)),llList2String(p,3),llList2String(p,2),vecty(llList2String(p,4)),vecty(llList2String(p,5)),floaty(llList2String(p,6))],"|")+"\"";
        }
    }
    page+=end;
    llSetText("Creating the script",<1,1,1>,1);
    create();
    
}

//map,stitch,offset,rot,texture,size

default
{

    touch_start(integer total_number)
    {
        if(llGetNumberOfPrims()>40)llOwnerSay("Warning! The object is primmy. If all of them are sculpts - proper work of script is not guaranteed.");
        llOwnerSay("Generating");
        generate();
    }
    link_message(integer sender,integer num,string body,key id){
    
        if(id=="pasteurl"){ // paste url returned        
            llOwnerSay(body);
    
        }
        else if(id=="error"){ // error returned
            llOwnerSay("Pastehtml API seem to be not working. The answer was:\n\n"+body);
        }
    
    }   
    http_response(key rid,integer status,list meta,string body){
     
        llSetText("",<1,1,1>,1);
        if(llSubStringIndex(body,"http://pastehtml.com/")!=0)return;
        llLoadURL(llGetOwner(),"The link with your script",body);         
        
    }     
}
