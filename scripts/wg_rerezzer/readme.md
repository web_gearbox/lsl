# ReRezzer

The ReRezzer is created for sculpt makers, who want to give their customers an option to rebuild their sculpted creations with customers' own prims. 

## What does it do?

It fetches information about all sculpted (only) prims in a linkset : their size, position, rotation and texture with all texture parameters and then generates a script, that, when clicked and if has appropriate object in its contents, rebuilds the linkset. 
       
It's easy : watch the [demo video](http://www.youtube.com/watch?v=7Jwf4eafx0s)

## How do I use it?

Follow 3 easy steps:

1. Drop generator and pastebin_api scripts in your sculpted linkset and click it. Note that if it has more than 40 prims - the work of script may be unstable due to secondlife memory limitations. Also because it's buggy. You are warned :P
2. After the script finishes its work - it should give you the link. Follow the link to open a webpage containing builder script.
3. Rez an object in sl. Name it, say, "Rezzer". Create a new script inside of it. Copy builder script from webpage, and paste it into the script inside the Rezzer.
       
That's all. Now you can give the rezzer object (with mod rights on prim) to your customers with child script so they can put child script it into their own prim, put the prim into your Rezzer and click it to rebuild your sculpt of their prim.
       
Don't forget to change permissions of the new script you created :P
       
## I'm still confused.

And again, I made a YouTube [demonstration video](http://www.youtube.com/watch?v=7Jwf4eafx0s). Watch it carefully, it's your last chance, hehehe.
       
## What are permissions of use?

You don't have to change permissions on our scripts. Decide yourself about permissions on your scripts you'll generate using our scripts :)
       
Don't forget to give child script to your customers ;-)