//©web.gearbox, 2011. http://webgearbox.tumblr.com

// For description of parameters and your API key - register at pastebin.com and visit http://pastebin.com/api
// You MUST use your API key. However you dont have to modify this wery script to set api key - you can set it
// remotely from the other script. See the description and the example script.

string api_dev_key="";   
string api_paste_expire_date="10M"; //N, 10M, 1H, 1D, 1M
string api_paste_format="text";
string api_paste_private="0"; 
string api_url="http://pastebin.com/api/api_post.php";                                                     
string paste_text="";
integer last_num=0;
                                                                       
string owner_name="";   

request(string r){
    if(api_dev_key==""){
        llRegionSayTo(llGetOwner(),DEBUG_CHANNEL,"No API key specified. You must type in your pastebin api key.");   
        return;
    }
    r="api_paste_format="+api_paste_format+"&api_dev_key="+api_dev_key+"&api_paste_expire_date="+api_paste_expire_date+"&"+r;
    llHTTPRequest(api_url,[HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded", HTTP_VERBOSE_THROTTLE, 0],r);
          
}  

default{
    
    
    link_message(integer sender, integer num, string text, key action){    
        
        if(action=="create"){    
            if(text=="")text=llDumpList2String(llParseString2List(llToLower(llKey2Name(llGetOwner())),[" "],[]),"_");       
            if(paste_text=="")return;
            request("api_option=paste&api_paste_name="+text+"&api_paste_code="+llEscapeURL(paste_text));
            paste_text="";
        }
        else if(action=="add")paste_text+=text;       
        else if(action=="clear")paste_text="";        
        else if(action=="format")api_paste_format=text;
        else if(action=="expiry")api_paste_expire_date=text;        
        else if(action=="key")api_dev_key=text;
        else if(action=="reset")llResetScript(); 
    
    }

    http_response(key rid,integer status,list meta,string body){
     
        if(llSubStringIndex(body,"http://pastebin.com/")!=0){
            llMessageLinked(-1,0,body,"error");    
            return;
        }
        string paste_key=llList2String(llParseString2List(body,["/"],[]),-1);
        llMessageLinked(-1,1,paste_key,"pastekey"); 
        llMessageLinked(-1,1,body,"pasteurl");         
        
    }
}
