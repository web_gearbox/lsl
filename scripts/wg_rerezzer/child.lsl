default
{
    on_rez(integer num){
        
        if(num)llListen(num,"",NULL_KEY,"");
        
    }

    listen(integer num,string name,key id,string msg){
        
        list params=llParseStringKeepNulls(msg,["|"],[]);
        key map=(key)llList2String(params,0);
        integer stitching=(integer)llList2String(params,1);
        vector off=(vector)llList2String(params,2);        
        rotation rot=(rotation)llList2String(params,3);                
        key texture=(key)llList2String(params,4);    
        vector size=(vector)llList2String(params,5);  
        vector texturerp=(vector)llList2String(params,6);  
        vector textureoffs=(vector)llList2String(params,7);          
        float texturerot=(float)llList2String(params,8);                  
        
        llSetLinkPrimitiveParamsFast(LINK_THIS,[PRIM_TEXTURE,-1,texture,texturerp,textureoffs,texturerot,PRIM_TYPE,PRIM_TYPE_SCULPT,map,stitching,PRIM_POSITION,llGetPos()+off,PRIM_ROTATION,rot,PRIM_SIZE,size]);
        llRemoveInventory(llGetScriptName());
        
    }
}
