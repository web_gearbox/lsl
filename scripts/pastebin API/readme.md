# Pastebin API

Welcome. This is a simple API that allows to create pastebin.com pastes via LSL. In order to make API work - put pastebin.api script in linkset your script is in and send it a linkmessage. API will answer via linkmessage too.

This API will require you to have pastebin.com API (developer) key. You can get one by registering at pastebin.com and visiting pastebin.com/api link.

## Commands

- "key" - Set current api key. This should be always set:

    llMessageLinked(-1,0,"API DEVELOPER KEY","key");
    

- "add" - Add some text to paste data.:

    llMessageLinked(-1,0,"TEXT","add");
    

- "format" - Set syntax highlighting format. Formats are listed at pastebin.com/api

    llMessageLinked(-1,0,"FORMAT","format");
    

- "expiry" - Set expiry date for paste. Also listed at pastebin.com/api

    llMessageLinked(-1,0,"DATE","expiry");
    

- "clear" - Clear current paste data.

    llMessageLinked(-1,0,"","clear");
    

- "create" - Upload paste to pastebin and clear paste data. If PASTENAME is empty - paste will have owner's name.
    llMessageLinked(-1,0,"PASTENAME","create");
    

- Reset all API settings. Including developer key.

    llMessageLinked(-1,0,"","reset");

## Answers

The API will return link_message data (sent at whole linkset). Key part of it will be the status of paste upload. If paste uploaded successfully - two link messages with following key part values will be raised:

- "pastekey" - text part is a key of paste uploaded
- "pasteurl" - text part is an url of paste uploaded

If upload returned error the key part will be "error" and the text part will be
pastebin.com server response.

## LSL code Example

    default{
        touch_start(integer total_number){
        
            // Setting API key. Get yours at pastebin.com/api. 
            llMessageLinked(-1,0,"8a38123a86e042a6770b11a89ff24cd7","key");
            
            // Setting syntax format to lsl2.
            llMessageLinked(-1,0,"lsl2","format");
            
            // Setting expiry to 10 minutes.
            llMessageLinked(-1,0,"10M","expiry");        
            
            // Clearing paste output text
            llMessageLinked(-1,0,"","clear");  
        
            // Adding some text to paste
            llMessageLinked(-1,0,"Hello, world!","add");
            
            // Uploading paste to pastebin.com
            llMessageLinked(-1,0,"Example paste","create");        
        }
                
        link_message(integer sender,integer num,string body,key id){        
                if(id=="pasteurl"){
                    llOwnerSay(body);
                }
                else if(id=="error"){
                    llOwnerSay("Pastebin seem to be not working. Pastebin answer was:\n\n"+body);    
                }
            }
        }
 

## Credits

This API was written by Web Gearbox on 2011.11.14.