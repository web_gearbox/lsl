//©web.gearbox, 2011. http://webgearbox.tumblr.com
//Script that uploads pastebin api description on pastebin and gives you the link

string apikey="yourkeyhere";    // paste your API key here!!!

default{

    touch_start(integer total_number){
        if(llDetectedKey(0)!=llGetOwner()){
            llRegionSayTo(llDetectedKey(0),0,"Don't touch what's not yours.");return;    
        }
        llMessageLinked(-1,0,apikey,"key");
        llMessageLinked(-1,0,"lsl2","format");
        llMessageLinked(-1,0,"10M","expiry");        
        llMessageLinked(-1,0,"","clear");  
        llMessageLinked(-1,0,"/*\nThis document was generated and uploaded from SL\n using pastebin.com API on "+(string)llGetDate()+" by "+(string)llKey2Name(llGetOwner())+"\n\n\n           --- Intro ---\n\n\nWelcome. This is a simple API that allows to create pastebin.com pastes via LSL.\nFor the API to work - put pastebin.api script in linkset your script is in and send it a linkmessage.\nAPI will answer via linkmessage too.\n\nThis API will require you to have pastebin.com developer key. You can get one by\nregistering at pastebin.com and visiting pastebin.com/api link.\n\n\n           --- Commands ---\n\n - Set current api key. This should be always set:\n   llMessageLinked(-1,0,\"<API DEVELOPER KEY>\",\"key\");\n\n - Set syntax highlighting format. Formats are listed at pastebin.com/api\n   llMessageLinked(-1,0,\"<FORMAT>\",\"format\");\n\n - Set expiry date for paste. Also listed at pastebin.com/api\n   llMessageLinked(-1,0,\"<DATE>\",\"expiry\");\n\n - Clear current paste data.\n   llMessageLinked(-1,0,\"\",\"clear\");\n\n - Add some text to paste data.:\n   llMessageLinked(-1,0,\"<TEXT>\",\"add\");\n\n - Upload paste to pastebin and clear paste data. If <PASTENAME> is empty - paste will\n   have owner's name.\n   llMessageLinked(-1,0,\"<PASTENAME>\",\"create\");\n\n - Reset all API settings. Including developer key.\n   llMessageLinked(-1,0,\"\",\"reset\");\n\n           --- Answers ---\n\nThe API will return link_message data (sent at whole linkset). Key part of it will be\nthe status of paste upload. If paste uploaded successfully - two link messages with\nfollowing key part values will be raised:\n\n - \"pastekey\" - text part is a key of paste uploaded\n - \"pasteurl\" - text part is an url of paste uploaded\n\nIf upload returned error the key part will be \"error\" and the text part will be\npastebin.com server response.\n \n           --- LSL code Example ---*/\n\n        \n        default\n        {\n        \n            touch_start(integer total_number){\n                \n            // Setting API key. Get yours at pastebin.com/api. \n                llMessageLinked(-1,0,\"8a38123a86e042a6770b11a89ff24cd7\",\"key\");\n        \n            // Setting syntax format to lsl2.\n                llMessageLinked(-1,0,\"lsl2\",\"format\");\n        \n            // Setting expiry to 10 minutes.\n                llMessageLinked(-1,0,\"10M\",\"expiry\");        \n        \n            // Clearing paste output text\n                llMessageLinked(-1,0,\"\",\"clear\");  \n        \n            // Adding some text to paste\n                llMessageLinked(-1,0,\"Hello, world!\",\"add\");\n        \n            // Uploading paste to pastebin.com\n                llMessageLinked(-1,0,\"Example paste\",\"create\");\n                \n            }\n                \n            link_message(integer sender,integer num,string body,key id){\n                \n                if(id==\"pasteurl\"){ // paste url returned\n                \n                    llOwnerSay(body);\n                    \n                }\n                else if(id==\"error\"){ // error returned\n                \n                    llOwnerSay(\"Pastebin API seem to be not working. Pastebin answer was:\\n\\n\"+body);    \n                    \n                }\n                \n            }\n        }\n\n\n\n/*         --- Credits ---\n\nThis API was made by Web Gearbox on 2011.11.14. Lalala.","add");
        llMessageLinked(-1,666,"LSL>Pastebin API information","create");
        
    }
        
    link_message(integer sender,integer num,string body,key id){
        
        if(id=="pastekey"){
        
            llOwnerSay("SL pastebin API Documentation has been uploaded. Follow http://pastebin.com/raw.php?i="+body+" link.");
            llLoadURL(llGetOwner(),"Open the link for API info.","http://pastebin.com/raw.php?i="+body);
            
        }
        else if(id=="error"){
        
            llOwnerSay("Pastebin API seem to be not working. Pastebin answer was:\n\n"+body);    
            
        }
        
    }
}
