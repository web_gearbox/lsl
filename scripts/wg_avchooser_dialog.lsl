// avatars dialog version 0.1 (alpha)
// ©web.gearbox, 2013. http://webgearbox.tumblr.com
// This script gives you the choice form avatars standing around
// Also can be triggered via chat
  
integer distance=20;                        // Radius in which avatars are detected
string command="fart";                      // Command to trigger action if name found.
                                            // Say, "fart gearbo" will trigger at Web Gearbox
string dialog_message="Chose your victim:"; // Text to display in dialog
 
// -- The action. This happens when agent is selected:
 
action(key victim){if(victim){
   
    llOwnerSay("You have farted on "+llKey2Name(victim));
 
}else return;}













 
// -- Avatars within radius - to a list
 
list getAvatarsNearby(float radius){
    list agents_temp=llGetAgentList(AGENT_LIST_REGION,[]);
    if(radius==0)return agents_temp;
    list agents_new=[];
    integer n=llGetListLength(agents_temp);
    vector ourpos=llGetPos();
    while(n--){
        key test=llList2Key(agents_temp, n);
        float dist=llVecDist(ourpos,llList2Vector(llGetObjectDetails(test, [OBJECT_POS]),0));
        if(dist<=radius && test!=llGetOwner())agents_new+=test;
    }
    return agents_new;
}
 
 
// -- Dialog
 
integer dialog_chan=0;
integer dialog_listen=0;
_llDialog(key id,string msg,list items){
    dialog_chan=-((integer)llFrand(0xDEAD)+0xFACE);
    llListenRemove(dialog_listen);
    dialog_listen=llListen(dialog_chan, "", id, "");
    llDialog(id, msg, items, dialog_chan);
}
 
 
// -- Get agent by a portion of name (from nearby)
 
key agentByName(string msg){
    list crowd=llGetAgentList(AGENT_LIST_REGION,[]);
    key victim;
    msg=llToLower(msg);
    integer n=llGetListLength(crowd);
    while(n--){
        key id=llList2String(crowd,n);
        string name=llKey2Name(id);
        if(id!=llGetOwner())if((~llSubStringIndex(llToLower(name),msg)) || (~llSubStringIndex(llToLower(llGetDisplayName(id)),msg))){
            if(victim){
                return NULL_KEY;
            }else victim=id;
        }
    }
    if(victim)return victim;
    return NULL_KEY;
}
 
// -- Keys to names
 
list keys2names(list keys){
    list names=[];
    integer n=llGetListLength(keys);
    while(n--)names+=llKey2Name((key)llList2String(keys, n));
    return names;
}
 
// -- Trim list of strings to 24 symbols.
 
list trimForButtons(list input){
    list buttons=[];
    integer n=llGetListLength(input);
    while(n--)buttons+=llGetSubString(llList2String(input, n),0,23);
    return buttons;
}
 
// -- Agent chooser
 
list agents;
integer lastpage=0;
 
string P_NEXT="Next >";
string P_PREV="< Prev";
string P_EMPTY="---";
 
chooser(integer page){
    integer total_agents=llGetListLength(agents);
    integer total_pages=llCeil((float)total_agents/9);
    if(total_agents<=12){
        _llDialog(llGetOwner(),dialog_message+" ",trimForButtons(keys2names(agents)));
        return;
    }
   
    if(page>=total_pages)page=0;if(page<0)page=total_pages - 1;
    lastpage=page;
    _llDialog(llGetOwner(),dialog_message+" ",[llList2String([P_EMPTY,P_PREV],page>0),P_EMPTY,llList2String([P_EMPTY,P_NEXT],page<total_pages)]+trimForButtons(keys2names(llList2List(agents, page*9, page*9+8))));
}
 
default{
   
    state_entry()
    {
        llSetMemoryLimit(24000);
 
        llListen(0, "", "", "");
        llListen(1, "", "", "");
    }
 
    listen(integer channel, string name, key id, string msg)
    {
        if(llGetOwnerKey(id)!=llGetOwner())return;
 
        if(channel==dialog_chan){
            llListenRemove(dialog_listen);
            if(msg==P_NEXT)chooser(lastpage+1);
            else if(msg==P_PREV)chooser(lastpage-1);
            else if(msg==P_EMPTY)chooser(lastpage);
            else action(agentByName(msg));
            return;
        }
       
        string cmd=llList2String(llParseString2List(msg,[" "],[""]),0);
        string args=llGetSubString(msg,llStringLength(cmd)+1,-1);
 
        cmd=llToLower(cmd);
        if(cmd==llToLower(command)){
            action(agentByName(args));
        }
    }
 
    touch_start(integer num){
        if(llDetectedKey(0)!=llGetOwner()){
            llSay(0,llGetObjectName()+" spanked "+llDetectedName(0)+"'s ass for touching objects that don't belong to them");
            return;   
        }
 
        
        if(distance && distance<=95){
            llSensor("", "", AGENT, distance, PI);
            return;
        }
 
        agents=getAvatarsNearby(distance);
        chooser(0);
    } 
    sensor(integer num){
        agents=[];
        while(num--)if(llDetectedKey(num)!=llGetOwner())agents+=llDetectedKey(num);
        chooser(0);
    }
}