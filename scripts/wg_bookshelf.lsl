// Bookshelf system script version 0.1
// (c) web.gearbox, 2013 http://webgearbox.tumblr.com

integer perms=2; // 0 for owner-only, 1 for group, 2 for everyone

integer current_page=0;
integer notecardnumber;
integer total_pages;

integer channel=31337;
integer listen_hdl;

list names_b;
list names_full;

list getbuttons(integer page){
 
    list buttons=[];
    if(total_pages>1){
        buttons+=["< Prev","-Access-","Next >"];        
    }
    else buttons+=[".","-Access-","."];

    integer i;
    names_b=[];
    names_full=[];    
    for(i=page*9;i<page*9+9;i++){
        if(llGetInventoryName(INVENTORY_NOTECARD,i)){
            string ncn=llGetInventoryName(INVENTORY_NOTECARD,i);
            string tr=llGetSubString(ncn,0,10);
            buttons+=tr;
            names_full+=ncn;
            names_b+=tr;
        }
    }
    return buttons;
    
}

wgDialog(key id,string text,list buttons){
 
    channel=-(integer)llFrand(20000+10000);
    llListenRemove(listen_hdl);
    listen_hdl=llListen(channel,"",id,"");
    llDialog(id,text,buttons,channel);
    
}
    

dialog(key id){ 
    notecardnumber=llGetInventoryNumber(INVENTORY_NOTECARD);
    total_pages=llCeil((float)notecardnumber/9);
    wgDialog(id,"Page "+(string)(current_page+1)+" of "+(string)total_pages,getbuttons(current_page));  
}

default
{
    touch_start(integer total_number)
    {
        if(llGetInventoryNumber(INVENTORY_NOTECARD))
        if(perms==2 | (perms==1 && llSameGroup(llDetectedKey(0))) | (perms==0 && llDetectedKey(0)==llGetOwner())){ 
            dialog(llDetectedKey(0));
        }
    }
    listen(integer channel,string name,key id,string msg){
        llListenRemove(listen_hdl);    
        if(msg=="< Prev"){
            if(current_page>0)current_page--;
            else current_page=total_pages-1;
            dialog(id);
        }
        else if(msg=="Next >"){
            if(current_page<total_pages-1)current_page++;
            else current_page=0;
            dialog(id);
          
        }            
        else if(msg=="-Access-"){
            if(id==llGetOwner()){
                wgDialog(id,"Select one:",["-Owner-","-Group-","-Anyone-"]); 
            }
            else{
                llInstantMessage(id,"Sorry, but this option is owner-only.");
            }
        } 
        else if(msg=="-Owner-"){
            perms=0;
            llInstantMessage(id,"Permissions set to owner only.");            
        } 
        else if(msg=="-Group-"){
            perms=1;
            llInstantMessage(id,"Permissions set to group.");            
        } 
        else if(msg=="-Anyone-"){
            perms=2;
            llInstantMessage(id,"Permissions set to anyone.");
        }               
        else{
            msg=llList2String(names_full,llListFindList(names_b,[msg]));
            if(llGetInventoryKey(msg)){
                llGiveInventory(id,msg);
            }
        }

        
    }
    
}
