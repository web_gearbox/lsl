//©web.gearbox, 2013. http://webgearbox.tumblr.com

// wg_touch_light 1.3

// Universal touch lamp script. Click changes light parameters
// of all prims which name matches specified.

vector cstart=<0,0,0>;      // Color in off state
vector cend=<1,1,1>;        // Color in on state

float gstart=0;             // Glow in off state
float gend=1;               // Glow in on state

float astart=1;             // Alpha in off state
float aend=1;               // Alpha in on state

vector light_color=<1,1,1>; // Light color
float light_intensity=1;    // Light intensity
float light_falloff=1;       // Light falloff
float light_radius=5;       // Light Radius 

string name="light";        // Name of affected prims
integer face=-1;            // Prim face affected
integer light_enable=1;     // Is light enabled     
integer fullbright_enable=0;// Is fullbright change enabled

integer toggle=1;

default{
    touch_start(integer total_number)
    {
        toggle=!toggle;
        list l=[];
                
        integer n=llGetNumberOfPrims()+1;
        while(--n)if(llToLower(llGetLinkName(n))==llToLower(name))l+=[PRIM_LINK_TARGET,n,PRIM_COLOR,face,llList2Vector([cstart,cend],toggle),llList2Float([astart,aend],toggle),PRIM_GLOW,face,llList2Float([gstart,gend],toggle),PRIM_POINT_LIGHT,toggle*light_enable,light_color,light_intensity,light_radius,light_falloff,PRIM_FULLBRIGHT,face,toggle*fullbright_enable];  

        if(llGetListLength(l))llSetLinkPrimitiveParamsFast(0,l);
    }
}
