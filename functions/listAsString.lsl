// ©web.gearbox, 2013. http://webgearbox.tumblr.com

// This function is used to show list as if it was in lsl code.
// Useful for scripts that generate other scripts :P

string floatAsString(float a){
  string s=(string)a;
  while(llGetSubString(s,-1,-1) =="0"){
      s=llGetSubString(s,0,-2);
      if(llGetSubString(s,-1,-1) ==".")return llGetSubString(s,0,-2);
  }
  return s;
}

string vectorAsString(vector a){
    return "<"+floatAsString(a.x)+", "+floatAsString(a.y)+", "+floatAsString(a.z)+">";  
}

string rotationAsString(rotation a){
    return "<"+floatAsString(a.x)+", "+floatAsString(a.y)+", "+floatAsString(a.z)+", "+floatAsString(a.s)+">";  
}


string stringAsString(string a){
    return "\""+llDumpList2String(llParseStringKeepNulls(llDumpList2String(llParseStringKeepNulls(llDumpList2String(llParseStringKeepNulls(llDumpList2String(llParseStringKeepNulls(a,["\\"],[]),"\\\\"),["\""],[]),"\\\""),["\t"],[]),"\\t"),["\n"],[]),"\\n")+"\"";       
    
}

string listAsString(list src){
        
        integer num=llGetListLength(src);
        integer i;
        list r=[];
        for(i=0;i<num;i++){
           
            integer type=llGetListEntryType(src,i);
            if(type==TYPE_FLOAT)r+=floatAsString(llList2Float(src,i));
            else if(type==TYPE_INTEGER)r+=(string)llList2Integer(src,i);
            else if(type==TYPE_VECTOR)r+=vectorAsString(llList2Vector(src,i));
            else if(type==TYPE_ROTATION)r+=rotationAsString(llList2Rot(src,i));  
            else if(type==TYPE_KEY || type==TYPE_STRING)r+=stringAsString(llList2String(src,i));
           
        }    
        
    return "["+llDumpList2String(r,", ")+"]";
}

default
{
    state_entry()
    {
        llOwnerSay(listAsString(["Hello, i'm a string. I have some \" quotes \" and even a tab or two:      ",1,23.41123,<12,32.4,0>,<1,2,3,0.3223>]));
    }

}
