// ©web.gearbox, 2010. http://webgearbox.tumblr.com

string text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur placerat egestas fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a erat a urna imperdiet bibendum eget non massa. Ut felis risus, pulvinar sit amet suscipit a, sollicitudin et mi. Cras ut pretium magna. Duis vel facilisis lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus purus iaculis est aliquam fringilla molestie orci euismod. Integer dignissim nunc elementum risus sodales at facilisis nibh rhoncus. Donec libero tortor, dignissim eget pharetra pharetra, ornare sed ligula. Nulla et leo in nibh consequat cursus eget non enim.";


list wgBreakStringPortional(string input_text,integer input_portion){
    

    integer total_portions=llCeil(llStringLength(input_text)/(float)input_portion);  

    integer current_portion=0;

    if(total_portions==1)return [input_text];

    list result=[];    
    
    for(current_portion=0;current_portion<total_portions;current_portion++)result+=llGetSubString(input_text,current_portion*input_portion,current_portion*input_portion+input_portion-1);
    
    return result;
    
    
}

default
{
    state_entry()
    {
        list strings=wgBreakStringPortional(text,50);
        
        llSetText(llDumpList2String(strings,"\n"),<0,1,0>,1);
    }


}
