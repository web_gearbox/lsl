// http://webgearbox.tumblr.com/post/14994256478/remembering-people-simplest-visitor-counter-script-and

list uuids=[];
integer count=0;

integer remember(key id){
    
    integer entry=(integer)("0x"+llGetSubString(id,0,7));
    integer found=llListFindList(uuids,[entry]);
    if(!~found){
        if(llGetListLength(uuids)>3500)uuids=llList2List(uuids,1,-1)+entry;
        else uuids+=entry;
        return ++count;
    }
    uuids=llDeleteSubList(uuids,found,found)+entry;
    return count;

}

default{
    state_entry(){
        llSensorRepeat("",NULL_KEY,AGENT,96,PI,1);
    }
    sensor(integer num){
        while(num--){
            llSetText((string)remember(llDetectedKey(num)),<1,1,1>,1);
        }
    }
}