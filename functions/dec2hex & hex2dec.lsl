string dec2hexA(integer dec){
    string r;
    while(dec){
        r=llUnescapeURL("%"+(string)((dec&15)+30+((dec&15)>9)))+r;
        dec=dec>>4;
    }
    
    if(r=="")return "0x0";
    return "0x"+r;
}


// [06:27]  Wintermute [Web Gearbox]: well but the function A has "the fantastic element of surprise"
// [06:27]  Takni [Takni Resident]: too bad the user will usually miss seeing the beauty in it



integer hex2dec(string hex){
    return (integer)hex;
}


list letters=["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"];

string dec2hex(integer dec){
    string r;
    while(dec){
        r=llList2String(letters,dec&0xf) + r;
        dec=dec>>4;
    }
    
    if(r=="")return "0x0";
    return "0x"+r;
}

