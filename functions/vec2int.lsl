integer toInt(integer x,integer y,integer z){
    
    if(x<0)x=0; if(x>255)x=255;
    if(y<0)y=0; if(y>255)y=255;
    if(z<0)z=0; if(z>255)z=255;
    
    return x | y<<8 | z<<16;
}

vector fromInt(integer i){
    return <(i&255),((i>>8)&255),((i>>16)&255)>;
}
