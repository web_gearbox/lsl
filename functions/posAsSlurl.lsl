string posAsSlurl(vector p){
    
    return "secondlife:///"+llEscapeURL(llGetRegionName())+"/"+(string)((integer)p.x)+"/"+(string)((integer)p.y)+"/"+(string)((integer)p.z);
    
}

default{
    state_entry(){
        llOwnerSay(posAsSlurl(llGetPos()));
    }
}
