main(integer mode,integer p){
    if(mode==EVENT_ENTRY){
        
        llRegionSayTo(llGetOwner(),0,"Hello, avatar!");
        
    }
    else if(mode==EVENT_TOUCH){
        
        llRegionSayTo(llDetectedKey(0),0,"Touched");
        
    }
}

// --- global variables



// --- constants and service vars

integer STATE_DEFAULT=0;
integer EVENT_ENTRY=0;integer EVENT_TOUCH=1;
integer TOUCH_START=0;
integer current_state=0;integer touch_state=0;

// --- states and events

default{
    state_entry(){current_state=STATE_DEFAULT;main(EVENT_ENTRY,0);}
    touch_start(integer t){touch_state=TOUCH_START;main(EVENT_TOUCH,t);}
}
