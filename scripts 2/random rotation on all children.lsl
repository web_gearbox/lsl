
default
{
    state_entry()
    {
        llSetMemoryLimit(5000);
        integer num=llGetNumberOfPrims()+1;
        while(--num-1)llSetLinkPrimitiveParamsFast(num,[PRIM_OMEGA,<llFrand(1),llFrand(1),llFrand(1)>,PI,PI]);
    }

}
 