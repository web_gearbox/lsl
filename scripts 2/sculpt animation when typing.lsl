list frames=[


    "ef7cb545-fea3-bb72-c59d-9e5f750c9386",
    "a62f6911-cf72-bde3-79bd-e07e1eb9069d",
    "ed12b660-de3a-dabe-6d8d-21b6871f4e44",
    "a62f6911-cf72-bde3-79bd-e07e1eb9069d",
    "ef7cb545-fea3-bb72-c59d-9e5f750c9386"

];

key still="ef7cb545-fea3-bb72-c59d-9e5f750c9386";

float delay=0.1;

integer started=0;
integer current_frame;
animate(){
    
 
    llSetLinkPrimitiveParamsFast(LINK_THIS,[PRIM_TYPE,PRIM_TYPE_SCULPT,llList2String(frames,current_frame),1]);
    if(delay)llSleep(delay);
    if(++current_frame>=llGetListLength(frames))current_frame=0;
    
}

default
{
    state_entry(){
        while(1){
            
            if(llGetAgentInfo(llGetOwner())&AGENT_TYPING){
            
                if(!started){
                    started=1;
                    current_frame=0;    
                }
                animate();
                
            }
            else{
                started=0;
                llSetLinkPrimitiveParamsFast(LINK_THIS,[PRIM_TYPE,PRIM_TYPE_SCULPT,still,1]);
            }
            
            
        }
    }
}
