go(key id,integer type){
 
    string n=llGetInventoryName(type,0);
    if(llGetInventoryType(n)!=type)return;
    llGiveInventory(id,n);   
    
}

default{
    touch_start(integer total_number){
        
        go(llDetectedKey(0),INVENTORY_NOTECARD);
        go(llDetectedKey(0),INVENTORY_OBJECT);
        go(llDetectedKey(0),INVENTORY_LANDMARK);
        go(llDetectedKey(0),INVENTORY_TEXTURE);
        
    }
}
