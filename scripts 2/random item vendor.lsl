// Scripted by Webby :P

integer PRICE=2000;//Price for an item.

string STR_WRONG_AMOUNT="Sorry, you paid wrong amount of money.";

refresh(){
    integer num=llGetInventoryNumber(INVENTORY_OBJECT);
    if(num)llSetPayPrice(PAY_HIDE,[PRICE,PAY_HIDE,PAY_HIDE,PAY_HIDE]);
    else llSetPayPrice(PAY_HIDE,[PAY_HIDE,PAY_HIDE,PAY_HIDE,PAY_HIDE]);    
}    

default
{
    state_entry(){
        refresh();
    }

    money(key id,integer amount){   
        if(amount!=PRICE){
            llRegionSayTo(id,0,STR_WRONG_AMOUNT);  
            return;
        }
        else{
            llGiveInventory(id,llGetInventoryName(INVENTORY_OBJECT,(integer)llFrand(llGetInventoryNumber(INVENTORY_OBJECT)+1)));
        }  
    }
    
    changed(integer change){
        if(change & CHANGED_INVENTORY)refresh();            
    }

}
