
default
{
    state_entry()
    {
        llSetMemoryLimit(5000);
        llSetTimerEvent(2);
    }
    timer(){
        llSetTimerEvent(llFrand(2)+2);
        integer num=llGetNumberOfPrims()+1;
        while(--num-1)llSetLinkPrimitiveParamsFast(num,[PRIM_OMEGA,<llFrand(1),llFrand(1),llFrand(1)>,PI,PI]);    
    }


}
 