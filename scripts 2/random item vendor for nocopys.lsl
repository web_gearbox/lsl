// Scripted by Webby :P

integer PRICE=2000;//Price for an item.

string STR_WRONG_AMOUNT="Sorry, you paid wrong amount of money.";
string STR_LEFT="meeros left.";
string STR_EMPTY="The basket is empty";

refresh(){
    
    integer num=llGetInventoryNumber(INVENTORY_OBJECT);
    if(num){
    
        llSetText((string)num+" "+STR_LEFT,<1,1,1>,1);
        llSetPayPrice(PAY_HIDE,[PRICE,PAY_HIDE,PAY_HIDE,PAY_HIDE]);
        
    }
    else{
    
        llSetText(STR_EMPTY,<1,1,1>,1);
        llSetPayPrice(PAY_HIDE,[PAY_HIDE,PAY_HIDE,PAY_HIDE,PAY_HIDE]);    
        
    }
}    

default
{
    state_entry()
    {
        refresh();
        
    }

    money(key id,integer amount){
        
        if(amount!=PRICE){
         
            
               llInstantMessage(id,STR_WRONG_AMOUNT);   
            
        }
        
        else{
         
            llGiveInventory(id,llGetInventoryName(INVENTORY_OBJECT,(integer)llFrand(llGetInventoryNumber(INVENTORY_OBJECT)+1)));
             
        }
        
        
    }
    
    changed(integer change){
     
        if(change & CHANGED_INVENTORY){
            
            refresh();
            
            
        }
        
        
    }

}
